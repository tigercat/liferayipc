# Liferay IPC for Vaadin 8 (OSGi)

## What is this

This is an unofficial version of Vaadin add-on which 
 is modified to support to work __in Liferay OSGi environment__.

Try this if you can't wait for the official update from the original developer. 

## Prerequirements

* Liferay >= 7.0  
  but I don't know if this works or not in 7.1 or higher.
* Vaadin >= 8.6.0 < 10  
  and modules must be deployed into Liferay previously.

## Download release

__Unofficial__ releases of this add-on are __not__ available at Vaadin Directory. ~~For Maven instructions, download and reviews, go to~~ ~~http://vaadin.com/addon/liferayipc~~

## Building and running demo

```
git clone https://gitlab.com/tigercat/liferayipc.git
cd liferayipc
mvn clean install
```

Note that you need to deploy the created modules using the Liferay Blade CLI tool, or copying jars into the directory of {liferay-dir}/deploy, or uploading from Application Manager web admin. Because this ```mvn liferay:deploy``` method was obsoleted after Liferay 7.

Deploy Vaadin LiferayIPC add-on, find:

* {build_dir}/liferayipc-addon/target/vaadin-ipc-for-liferay-3.0-SNAPSHOT.jar

Deploy Demo portlets if you want to try now, find:

* {build_dir}/liferayipc-demo/target/vaadin-ipc-for-liferay-demo-3.0-SNAPSHOT.jar

To see the demo, start Liferay and add the portlets to a page

### ** Something trouble?

The OSGi integration of Liferay and Vaadin 8.0~8.6.2 is still buggy now... I recommend restarting vaadin-shared module or restarting Liferay after deploying.

See: https://github.com/vaadin/framework/issues/10220

Hope PR 11334 and 11335 will be merged in new release of Vaadin.

